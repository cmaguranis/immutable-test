const fs = require("fs");
const { program } = require("commander");
const { assignEmployeesForProfit } = require("./parkingService");

program
  .version("0.0.1")
  .arguments("<inputFile>")
  .description("Parking system for calculating maximum profit", {
    inputFile: "JSON file containing parking data. Full or relative paths accepted",
  })
  .action((inputFile) => {
    try {
      const data = fs.readFileSync(inputFile, "utf8");
      const inputJson = JSON.parse(data);
      const output = assignEmployeesForProfit(inputJson);

      console.log(JSON.stringify(output, null, 2));
    } catch (err) {
      console.error(err);
    }
  });

program.parse(process.argv);
