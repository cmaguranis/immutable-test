const orderBy = require("lodash/orderBy");

const REFUEL_THRESHOLD_PERCENT = 0.1;
const FUEL_PRICE_PER_LITRE = 1.75;
const PARKING_COST_SMALL = 25.0;
const PARKING_COST_LARGE = 35.0;
const DECIMAL_PLACES = 2;

// Not used but still important to know
// const EMPLOYEE_A_COMMISSION_PERCENT = 0.11;
// const EMPLOYEE_B_COMMISSION_PERCENT = 0.15;

const calculateParkingPrices = (vehicles) =>
  vehicles.map(({ size, fuel, ...rest }) => {
    let price = size === "small" ? PARKING_COST_SMALL : PARKING_COST_LARGE;

    const { capacity, level } = fuel;
    let fuelAdded = 0.0;

    // Can refuel
    if (level <= REFUEL_THRESHOLD_PERCENT) {
      fuelAdded = parseFloat(((1 - level) * capacity).toFixed(DECIMAL_PLACES));
      price = price + parseFloat((fuelAdded * FUEL_PRICE_PER_LITRE).toFixed(DECIMAL_PLACES));
    }

    return {
      ...rest,
      size,
      fuel,
      fuelAdded,
      price,
    };
  });

const assignEmployeesForProfit = (vehicles = []) => {
  if (vehicles.length < 1) {
    return [];
  }

  const vehiclesWithPrice = calculateParkingPrices(vehicles);
  const vehiclesSortedByPrice = orderBy(vehiclesWithPrice, ["price"], ["desc"]);
  const employeeAssignmentSplit = Math.floor((vehiclesSortedByPrice.length - 1) / 2);
  return vehiclesSortedByPrice.map(({ licencePlate, fuelAdded, price }, index) => {
    // Assign the cheaper employee to the more expensive parking spots
    return {
      licencePlate,
      fuelAdded,
      price,
      employee: index > employeeAssignmentSplit ? "B" : "A",
    };
  });
};

module.exports = {
  assignEmployeesForProfit,
  PARKING_COST_SMALL,
  PARKING_COST_LARGE,
};
