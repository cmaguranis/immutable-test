const {
  assignEmployeesForProfit,
  PARKING_COST_SMALL,
  PARKING_COST_LARGE,
} = require("./parkingService");
const testData = require("./input.json");

test("no input returns empty array", () => {
  expect(assignEmployeesForProfit()).toEqual([]);
});

test("even number of parking spots equally splits employee assignment", () => {
  const output = assignEmployeesForProfit(testData);
  const employeeACount = output.filter((o) => o.employee === "A").length;
  const employeeBCount = output.filter((o) => o.employee === "B").length;

  expect(employeeACount === employeeBCount).toBe(true);
});

test("odd number of parking spots favors cheaper employee (A) assignment", () => {
  const input = [
    ...testData,
    {
      licencePlate: "K",
      size: "small",
      fuel: {
        capacity: 45,
        level: 0.5,
      },
    },
  ];
  const output = assignEmployeesForProfit(input);
  const employeeACount = output.filter((o) => o.employee === "A").length;
  const employeeBCount = output.filter((o) => o.employee === "B").length;

  expect(employeeACount === employeeBCount).toBe(false);
  expect(employeeACount > employeeBCount).toBe(true);
  // Deviation should not exceed one assignment
  expect(employeeACount === employeeBCount + 1).toBe(true);
});

test("fuel added only if at 10% or less", () => {
  const inputNoFuelAdded = [
    {
      licencePlate: "L",
      size: "small",
      fuel: {
        capacity: 45,
        level: 0.5,
      },
    },
  ];
  expect(assignEmployeesForProfit(inputNoFuelAdded)[0].fuelAdded).toEqual(0);

  const inputFuelAdded = [
    {
      licencePlate: "M",
      size: "large",
      fuel: {
        capacity: 22,
        level: 0.09,
      },
    },
  ];
  expect(assignEmployeesForProfit(inputFuelAdded)[0].fuelAdded > 0).toBe(true);

  const inputFuelAddedLimit = [
    {
      licencePlate: "N",
      size: "large",
      fuel: {
        capacity: 22,
        level: 0.1,
      },
    },
  ];
  expect(assignEmployeesForProfit(inputFuelAddedLimit)[0].fuelAdded > 0).toBe(true);
});

test("it assigns the correct value to small and large vehicles", () => {
  const smallVehicle = [
    {
      licencePlate: "O",
      size: "small",
      fuel: {
        capacity: 45,
        level: 0.5,
      },
    },
  ];
  expect(assignEmployeesForProfit(smallVehicle)[0].price).toEqual(PARKING_COST_SMALL);

  const largeVehicle = [
    {
      licencePlate: "P",
      size: "large",
      fuel: {
        capacity: 87,
        level: 0.19,
      },
    },
  ];
  expect(assignEmployeesForProfit(largeVehicle)[0].price).toEqual(PARKING_COST_LARGE);
});
