# immutable-test

Parking service system

## Assumptions

I made a few assumptions on the test which I've outlined below:

- The input data is fixed and not a stream of data over time. This would require a much different approach and large investment in time to develop a viable heuristic.
- In the event that there is an odd number of parking spots to be processed, the system can arbitrarily assign either employee.
- Output order does not need to be the same as input (though easily done).
- `price` is how much is _charged to the customer_ in a parking spot, **not** `profit` made by the company.
- Employee commissions are not used besides knowing which is cheaper to assign, since no `profit` is calculated directly into the output.
- `level` is a percentage of the `capacity`.

## Try it out

Run either

```sh
node ./index.js <inputFile>
```

or

```sh
npm run parkingService -- <inputFile>
```

The sample input is included as `input.json`:

```sh
npm run parkingService -- ./input.json
```

## Unit tests

```sh
npm test
```
